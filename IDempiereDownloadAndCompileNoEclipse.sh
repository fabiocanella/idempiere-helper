#!/bin/bash 

#############################################################
# Author: Fabio Canella
# Company: Effecitech
# This script download IDempiere source and compile the code
#

#Java 8 Required
JAVA_HOME=/usr/local/jdk1.8.0_151
BASEDOWNLOAD=/opt/idempiere
IDBUCKCOMPILE=$BASEDOWNLOAD/compile
DIRECTOR_HOME=$BASEDOWNLOAD/compile/director
BUCKMINSTER_HOME=$BASEDOWNLOAD/compile/buckminster

#Path of idempiere sources
IDSRC=$IDBUCKCOMPILE/sources

PATH=$JAVA_HOME/bin:$PATH


IDUSER=utente
sudo mkdir -p $IDBUCKCOMPILE

sudo chown -R $IDUSER:$IDUSER $IDBUCKCOMPILE
cd $IDBUCKCOMPILE
mkdir sources
cd sources
hg clone https://bitbucket.org/idempiere/idempiere

hg clone idempiere testheadless

mkdir $DIRECTOR_HOME
cd $IDBUCKCOMPILE
wget https://github.com/hengsin/headless/raw/master/director_latest.zip
unzip director_latest.zip -d $IDBUCKCOMPILE

cd $IDBUCKCOMPILE/director
./director -r https://github.com/hengsin/headless/raw/master/4.5/ -d $BUCKMINSTER_HOME -p Buckminster -i org.eclipse.buckminster.cmdline.product

mkdir $BUCKMINSTER_HOME
cd $BUCKMINSTER_HOME
./buckminster install https://github.com/hengsin/headless/raw/master/4.5/ org.eclipse.buckminster.core.headless.feature

./buckminster install https://github.com/hengsin/headless/raw/master/4.5/ org.eclipse.buckminster.pde.headless.feature

./buckminster install https://github.com/hengsin/headless/raw/master/4.5/ org.eclipse.buckminster.maven.headless.feature


#Create target platform
./buckminster -data $IDSRC/testheadless importtargetdefinition -A "$IDSRC/testheadless/org.adempiere.sdk-feature/build-target-platform.target"

#Materialize
./buckminster -L DEBUG -consoleLog -data $IDSRC/testheadless import -P $IDSRC/testheadless/org.adempiere.sdk-feature/materialize.properties "$IDSRC/testheadless/org.adempiere.sdk-feature/adempiere.cquery"

#Build
#This have to take several minutes
cd $BUCKMINSTER_HOME
./buckminster -L DEBUG -consoleLog -data $IDSRC/testheadless build -t

./buckminster -L DEBUG -consoleLog -data $IDSRC/testheadless perform -P $IDSRC/testheadless/org.adempiere.server-feature/buckminster_linux_gtk_x86_64.properties 'org.adempiere.server:eclipse.feature#site.p2'


#Create product.zip
./buckminster -L DEBUG -consoleLog -data $IDSRC/testheadless perform  -P $IDSRC/testheadless/org.adempiere.server-feature/buckminster_linux_gtk_x86_64.properties   'org.adempiere.server:eclipse.feature#create.product.zip'




